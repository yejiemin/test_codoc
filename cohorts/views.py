from urllib import response
from rest_framework import viewsets,status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticatedOrReadOnly

from cohorts.models import Cohort
from cohorts.serializers import CohortSerializer
from commons.permissions import ReadOnly, IsAuthenticated
import logging


class CohortViewSet(viewsets.ModelViewSet):
    queryset = Cohort.objects.all()
    serializer_class = CohortSerializer
    permission_classes = [IsAuthenticatedOrReadOnly, ReadOnly | IsAuthenticated]
    def destroy(self, request,pk):
        try:
            cohort = Cohort.objects.get(id=pk)
        except Cohort.DoesNotExist:
            return Response({"err":"cohort doesn't exist"},status=status.HTTP_404_NOT_FOUND)
        #seul le propriétaire et les superutilisateurs peuvent supprimer une cohorte
        if request.user.id == cohort.owner.id or request.user.is_superuser :
            cohort.delete()
            return Response(status.HTTP_204_NO_CONTENT)
        # Sinon, return une message pour informer à l'utilisateur qu'il ne peut pas le supprimer
        else:
            return Response({"err":"You don't have permission to delete this cohort"},status=status.HTTP_403_FORBIDDEN)

