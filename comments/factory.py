import factory

from comments.models import Comment
from commons.factories import UserFactory


class CommentFactory(factory.django.DjangoModelFactory):
    name = factory.Faker('word')
    description = factory.Faker('text')
    owner = factory.SubFactory(UserFactory)

    class Meta:
        model = Comment