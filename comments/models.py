from django.contrib.auth.models import User
from django.db import models

from patients.models import Patient
from cohorts.models import Cohort


class Comment(models.Model):
    description = models.TextField()
    owner = models.ForeignKey(User,on_delete=models.CASCADE)
    # Si le patient ou la cohorte est supprimé, la commentaire est également supprimé
    patient = models.ForeignKey(Patient,on_delete=models.CASCADE)
    cohort = models.ForeignKey(Cohort,on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)