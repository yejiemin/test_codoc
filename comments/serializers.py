from urllib import request
from django.contrib.auth.models import User
from rest_framework import serializers

from cohorts.models import Cohort
from patients.models import Patient
from comments.models import Comment

class UserFilteredPrimaryKeyRelatedField(serializers.PrimaryKeyRelatedField):
    '''
    Filter pour "owner"
    Owner est l'utilisateur actuel (current user)
    '''
    def get_queryset(self):
        request = self.context.get('request', None)
        queryset = super(UserFilteredPrimaryKeyRelatedField, self).get_queryset()
        if not request or not queryset:
            return None
        return queryset.filter(id=request.user.id)
class CohortFilteredPrimaryKeyRelatedField(serializers.PrimaryKeyRelatedField):
    '''
    Un médecin ne peut ajouter des commentaires qu'aux patients qui appartiennent à ses cohortes.
    Mais superusers peuvent ajouter des commentaires pour n'importe quel patient, qu'il appartienne ou non à leurs cohortes.
    Donc il faut filter les cohortes
    '''
    def get_queryset(self):
        request = self.context.get('request',None)
        queryset = super(CohortFilteredPrimaryKeyRelatedField, self).get_queryset()
        if not request or not queryset:
            return None
        if request.user.is_superuser:
            return queryset
        return queryset.filter(owner = request.user.id)

class CommentSerializer(serializers.ModelSerializer):
    owner = UserFilteredPrimaryKeyRelatedField(queryset=User.objects)
    patient = serializers.PrimaryKeyRelatedField(queryset=Patient.objects.all())
    cohort = CohortFilteredPrimaryKeyRelatedField(queryset=Cohort.objects)
    

    def validate(self,data):
        '''
        Vérifier si le patient est dans la cohorte
        '''
        flag = False
        if type(data['cohort']) is not str:
            cohort_data = data['cohort'].id
        else:
            cohort_data = int(data['cohort'])
        if type(data['cohort']) is not str:
            patient_data = data['patient'].id
        else:
            patient_data = int(data['patient'])
        list_cohort_patient = Cohort.objects.values('id','patients')
        for item in list_cohort_patient:
            if cohort_data == item['id'] and patient_data == item["patients"]:
                # le patient appartient à la cohorte
                flag = True
        # si flag egale False, c'est à dire, le patient n'appartient pas le cohort
        if flag == False:
            raise serializers.ValidationError("The patient isn't in the cohort")
        return data

    class Meta:
        model = Comment
        fields = (
            'id', 'description', 'owner', 'patient','cohort','created_at', 'updated_at'
        )
