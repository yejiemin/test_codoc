from rest_framework import viewsets,status
from rest_framework.response import Response
from commons.permissions import  IsOwner,IsAuthenticated
from comments.models import Comment
from comments.serializers import CommentSerializer


class CommentViewSet(viewsets.ModelViewSet):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    permission_classes = [IsOwner,IsAuthenticated]

    def list(self, request, *args, **kwargs):
        '''
        override function list()
        Pour superuser, affichier tous les commentaires.
        Pour utilisateur normal, affichier ses commentaires.
        '''
        if request.user.is_superuser:
            self.querset = Comment.objects.all()
        else:
            self.queryset = Comment.objects.filter(owner=request.user.id)
        self.serializer_class = CommentSerializer
        return super(CommentViewSet, self).list(request, *args, **kwargs)
    
    def create(self, request, *args, **kwargs):
        '''
        override function create()
        Avant de créer un objet, il faut vérifier si le patient appartient à ses cohorts
        '''
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.validate(data=request.data)
        
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
