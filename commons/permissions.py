from rest_framework import permissions
from django.contrib.auth.models import User
from comments.models import Comment

class ReadOnly(permissions.BasePermission):

    def has_permission(self, request, view):
        return request.method in permissions.SAFE_METHODS

    def has_object_permission(self, request, view, obj):
        return request.method in permissions.SAFE_METHODS


class IsAuthenticated(permissions.BasePermission):

    def has_permission(self, request, view):
        return request.user.is_authenticated

    def has_object_permission(self, request, view, obj):
        return request.user.is_authenticated


class IsSuperUser(permissions.BasePermission):

    def has_permission(self, request, view):
        return request.user.is_superuser

    def has_object_permission(self, request, view, obj):
        return request.user.is_superuser

class IsOwner(permissions.BasePermission):
    '''
    Class-level permission, seul le propriétaire peut faire toute demande 
    '''
    def has_permission(self, request, view):
        if not view.kwargs:
            if view.action in ['list','create']:
                return True
        else:
            try :
                comment = Comment.objects.get(pk = view.kwargs['pk'])
            #Bien que le comment n'existe pas, on a permission.
            except Comment.DoesNotExist:
                return True
            return request.user == Comment.objects.get(pk = view.kwargs['pk']).owner  
    '''
    object-level permission, seul le propriétaire peut faire toute demande 
    '''   
    def has_object_permission(self, request, view, obj):
        if not view.kwargs:
            if view.action in ['list','create']:
                return True
        else:
            try :
                comment = Comment.objects.get(pk = view.kwargs['pk'])
            except Comment.DoesNotExist:
                return True
            return request.user == Comment.objects.get(pk = view.kwargs['pk']).owner  